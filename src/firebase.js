// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBW667QjNGXR7n7-gWMxGvI6QvtTvy649g",
  authDomain: "shop-c61cf.firebaseapp.com",
  projectId: "shop-c61cf",
  storageBucket: "shop-c61cf.appspot.com",
  messagingSenderId: "524544478780",
  appId: "1:524544478780:web:afc3d3d7a72a19eabe7d39"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);


export default app;